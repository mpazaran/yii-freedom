________________________________________________________________________________________________

                                Licencia Freedom
________________________________________________________________________________________________

Derechos de autor reservados por:
    - Mauricio Pazaran Lemus (mauricio.pazaran@gmail.com)
________________________________________________________________________________________________

"Freedom" es un software de código abierto no gratuito propiedad de Mauricio Pazaran Lemus
nombrado en adelante como propietario de los derechos, que incluye ademas ademas del código 
fuente materiales multimedia como pero no limitados a imagenes, audio y videos, con registro y 
pantente en trámite, Freedom se distribuye bajo los siguientes terminos.

La redistribución y/o el uso del código fuente en cualquier modalidad con o sin modificaciones 
no están permitidos sin la autorización del propietario de los derechos de autor.

El uso no autorizado de este software es una declaración de culpabilidad por parte del dueño
o dueños del dominio o dominios, ip y o servidor donde la plataforma se detecte en funcionamiento
del cargo de piratería de software o el termino legal equivalente al marco jurídico al que este 
apegado el presunto(s) responsable(s), por cual otorga al dueño de los derechos de autor a 
proceder conforme a derecho del marco jurídico pertinente y que más convenga a los intereses 
particulares del propietario de los derechos de autor.

La redistribución y/o el uso del código fuente en cualquier modalidad con o sin modificaciones
con autorización del propietario de los derechos de autor se apegan a las siguientes
condiciones:

 * Redistribución del código debe conservar las declaraciones de derechos de autor
   descritos anteriormente,
 * Redistribuciones en forma binaria debe reproducir las declaraciones de derechos
   de autor escritas anteriormente, está lista de declaraciones se debe incluir en todos
   los materiales distribuidos incluidos la documentación o imágenes o cualquier otro
   material incluido en la distribución.
 * El nombre "Freedom" no puede usarse para endorsar o promover productos derivados del uso de la 
   misma sín autorización expresa del autor.

Esté software es proporcionado por los dueños de los derechos de autor y se distribuye
con una garantía de funcionalidad limitada a componentes propios de la plataforma y su
interopeativilidad con software de terceros incluidos en la distribución.

Los componentes de software de terceros usados por este software se distribuyen "COMO SON"
y no cuentan con garantía alguna por parde de los propietarios de derechos de autor, por lo
cual no se incluye ninguna de las garantías implícitas de comerciabilidad.

En la adecuación de la plataforma para un propósito en particular, en ningún caso el
propietario de derechos de autor será responsable por cualquier daño directo, indirecto,
incidentales, especiales, ejemplares o consecuentes (incluidos, pero sin limitarse a,la
adquisición de bienes o servicios sustitutivos; pérdida de uso, datos o beneficios; o
interrupción de negocios) causados o sobre cualquier teoría de responsabilidad, ya sea en
contrato, estricta responsabilidad o agravio (incluyendo negligencia o cualquier otra)
que surjan en cualquier forma del uso de este software, incluso si se ha advertido
posibilidad de dicho daño.
